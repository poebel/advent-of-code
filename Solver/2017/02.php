<?php

class Solver_2017_02
{
    public function solve1($input)
    {
        $input  = trim($input);
        $input  = explode("\n", $input);
        $result = 0;

        foreach ($input as $row) {
            $row    = explode("\t", $row);
            $result += (max($row) - min($row));
        }

        return $result;
    }

    public function solve2($input)
    {
        $input  = trim($input);
        $input  = explode("\n", $input);
        $result = 0;

        foreach ($input as $row) {
            $row = explode("\t", $row);

            foreach ($row as $outerIdx => $dividend) {
                foreach ($row as $innerIdx => $divisor) {
                    if ($outerIdx != $innerIdx && $dividend % $divisor === 0) {
                        //echo $dividend . ' / ' . $divisor . PHP_EOL;
                        $result += ($dividend / $divisor);
                    }
                }
            }
        }

        return $result;
    }
}
