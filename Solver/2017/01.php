<?php

class Solver_2017_01
{
    public function solve1($input)
    {
        $input         = str_split(trim($input));
        $result        = 0;
        $digit         = null;
        $firstDigit    = null;
        $previousDigit = null;
        foreach ($input as $digit) {
            if (is_null($firstDigit)) {
                $firstDigit    = $digit;
                $previousDigit = $digit;
                continue;
            }
            if ($previousDigit == $digit) {
                $result += $digit;
            }
            $previousDigit = $digit;
        }
        if (!is_null($firstDigit) && $firstDigit == $digit) {
            $result += $firstDigit;
        }

        return $result;
    }

    public function solve2($input)
    {
        $input     = str_split(trim($input));
        $count     = count($input);
        $maxIdx    = $count - 1;
        $halfCount = round($count / 2);

        $result = 0;

        foreach ($input as $idx => $digit) {
            $compIdx = $idx + $halfCount;
            if ($compIdx > $maxIdx) {
                $compIdx -= $count;
            }

            if ($digit == $input[$compIdx]) {
                $result += $digit;
            }
        }

        return $result;
    }
}
