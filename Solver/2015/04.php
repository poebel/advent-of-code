<?php

class Solver_2015_04
{
    public function solve1($input)
    {
        return $this->_solve($input, '00000');
    }

    public function solve2($input)
    {
        return $this->_solve($input, '000000');
    }

    protected function _solve($input, $match)
    {
        $i = 1;
        while (true) {
            if (strpos(md5($input . $i), $match) === 0) {
                return $i;
            }
            $i++;
        }
    }
}
