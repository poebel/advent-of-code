<?php

class Solver_2015_02
{
    public function solve1($input)
    {
        $input = explode("\n", $input);

        $squareFeet = 0;

        foreach ($input as $dimensions) {
            $dimensions = explode('x', $dimensions);
            $l          = array_shift($dimensions);
            $w          = array_shift($dimensions);
            $h          = array_shift($dimensions);

            $areas = array(
                2 * $l * $w,
                2 * $w * $h,
                2 * $h * $l,
            );

            $smallestArea = min($areas) / 2;

            $areas[] = $smallestArea;

            $squareFeet += array_sum($areas);
        }

        return $squareFeet;
    }

    public function solve2($input)
    {
        $input = explode("\n", $input);

        $feet = 0;

        foreach ($input as $dimensions) {
            $dimensions = explode('x', $dimensions);
            sort($dimensions);
            $min = array_shift($dimensions);
            $mid = array_shift($dimensions);
            $max = array_shift($dimensions);

            $feet += (2 * $min + 2 * $mid) + ($min * $mid * $max);
        }

        return $feet;
    }
}
