<?php

class Solver_2015_05
{
    public function solve1($input)
    {
        $result = 0;

        $input = explode("\n", $input);

        foreach ($input as $row) {
            // At least 3 vowels
            if (preg_match_all('/[aeiou]/i', $row) < 3) {
                continue;
            }

            // Duplicate characters
            if (!preg_match('/(.)\\1/', $row)) {
                continue;
            }

            // No bad strings
            if (preg_match('/ab|cd|pq|xy/i', $row)) {
                continue;
            }

            $result += 1;
        }

        return $result;
    }

    public function solve2($input)
    {
        $result = 0;

        $input = explode("\n", $input);

        foreach ($input as $row) {
            // Duplicate characters twice
            if (!preg_match('/(.{2}).*\\1/', $row)) {
                continue;
            }

            // Same letter with spacer
            if (!preg_match('/(.).\\1/', $row)) {
                continue;
            }

            $result += 1;
        }

        return $result;
    }
}
