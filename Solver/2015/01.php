<?php

class Solver_2015_01
{
    public function solve1($input)
    {
        return (0 + substr_count($input, '(') - substr_count($input, ')'));
    }

    public function solve2($input)
    {
        $cur = 0;

        $inputArray = str_split($input);

        foreach ($inputArray as $pos => $dir) {
            if ($dir == '(') {
                $cur++;
            } else {
                $cur--;
            }

            if ($cur == -1) {
                return ($pos + 1);
            }
        }

        return 'ERROR';
    }
}
