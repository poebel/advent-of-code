<?php

class Solver_2015_06
{
    protected $_lightArray = array();

    public function solve1($input)
    {
        return $this->_solve($input, '_changeState1', 'count');
    }

    public function solve2($input)
    {
        return $this->_solve($input, '_changeState2', 'array_sum');
    }

    protected function _solve($input, $changeStateMethod = '_changeState1', $resultFunction = 'count')
    {
        $this->_lightArray = array();

        ini_set('memory_limit', '1G');

        $input = explode("\n", $input);
        foreach ($input as $row) {
            preg_match('/([a-z\s]+)/i', $row, $action);
            $action = trim($action[1]);

            preg_match('/(\d+,\d+).*?(\d+,\d+)/', $row, $range);
            $rangeFrom = explode(',', trim($range[1]));
            $rangeTo   = explode(',', trim($range[2]));

            for ($x = $rangeFrom[0]; $x <= $rangeTo[0]; $x++) {
                for ($y = $rangeFrom[1]; $y <= $rangeTo[1]; $y++) {
                    $this->$changeStateMethod($x . '|' . $y, $action);
                }
            }
        }

        return $resultFunction($this->_lightArray);
    }

    protected function _changeState1($key, $action)
    {
        switch ($action) {
            case 'turn on':
                $this->_lightArray[$key] = true;
                break;
            case 'turn off':
                unset($this->_lightArray[$key]);
                break;
            case 'toggle':
                if (isset($this->_lightArray[$key])) {
                    unset($this->_lightArray[$key]);
                } else {
                    $this->_lightArray[$key] = true;
                }
                break;
        }
    }

    protected function _changeState2($key, $action)
    {
        if (!isset($this->_lightArray[$key])) {
            $this->_lightArray[$key] = 0;
        }
        switch ($action) {
            case 'turn on':
                $this->_lightArray[$key] += 1;
                break;
            case 'turn off':
                $this->_lightArray[$key] -= 1;
                break;
            case 'toggle':
                $this->_lightArray[$key] += 2;
                break;
        }

        if ($this->_lightArray[$key] < 1) {
            unset($this->_lightArray[$key]);
        }
    }
}
