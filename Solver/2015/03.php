<?php

class Solver_2015_03
{
    protected $_coordX = 0;
    protected $_coordY = 0;
    protected $_directionCalc
                       = array(
            '^' => array('coord' => 'Y', 'operator' => '+'),
            '>' => array('coord' => 'X', 'operator' => '+'),
            'v' => array('coord' => 'Y', 'operator' => '-'),
            '<' => array('coord' => 'X', 'operator' => '-'),
        );

    protected $_visitedLocations = array('0|0' => true);

    public function solve1($input)
    {
        $this->_reset();

        return $this->_solve($input);
    }

    public function solve2($input)
    {
        $this->_reset();
        $this->_solve($input, 'odd');

        $this->_reset(false);
        $this->_solve($input, 'even');

        return count($this->_visitedLocations);
    }

    protected function _reset($withLocations = true)
    {
        $this->_coordX = 0;
        $this->_coordY = 0;
        if ($withLocations) {
            $this->_visitedLocations = array('0|0' => true);
        }
    }

    protected function _solve($input, $skip = false)
    {
        $input = str_split($input);

        foreach ($input as $i => $direction) {
            if ($skip == 'odd' && ($i % 2)) {
                continue;
            }
            if ($skip == 'even' && !($i % 2)) {
                continue;
            }

            $this->_move($direction);
        }

        return count($this->_visitedLocations);
    }

    protected function _move($direction)
    {
        $moveDefinition = $this->_directionCalc[$direction];

        $coordName = '_coord' . $moveDefinition['coord'];

        if ($moveDefinition['operator'] == '+') {
            $this->$coordName += 1;
        } else {
            $this->$coordName -= 1;
        }

        $this->_visitedLocations[$this->_coordX . '|' . $this->_coordY] = true;
    }
}
