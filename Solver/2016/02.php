<?php

class Solver_2016_02
{
    protected $_coordX = null;
    protected $_coordY = null;
    protected $_matrix = null;

    protected $_matrix1
        = array(
            '0|0' => '7',
            '0|1' => '4',
            '0|2' => '1',
            '1|0' => '8',
            '1|1' => '5',
            '1|2' => '2',
            '2|0' => '9',
            '2|1' => '5',
            '2|2' => '3',
        );

    protected $_matrix2
        = array(
            '0|2' => '5',
            '1|1' => 'A',
            '1|2' => '6',
            '1|3' => '2',
            '2|0' => 'D',
            '2|1' => 'B',
            '2|2' => '7',
            '2|3' => '3',
            '2|4' => '1',
            '3|1' => 'C',
            '3|2' => '8',
            '3|3' => '4',
            '4|2' => '9',
        );

    public function solve1($input)
    {
        $this->_coordX = 1;
        $this->_coordY = 1;
        $this->_matrix = $this->_matrix1;

        return $this->_solve($input);
    }

    public function solve2($input)
    {
        $this->_coordX = 0;
        $this->_coordY = 2;
        $this->_matrix = $this->_matrix2;

        return $this->_solve($input);
    }

    protected function _solve($input)
    {
        $input = explode("\n", $input);

        $result = '';

        foreach ($input as $row) {
            $row = str_split($row);
            foreach ($row as $direction) {
                $this->_move($direction);
            }

            $result .= $this->_currentPin();
        }

        return $result;
    }

    protected function _currentPin()
    {
        return $this->_matrix[$this->_coordX . '|' . $this->_coordY];
    }

    protected function _move($direction)
    {
        switch ($direction) {
            case 'U':
                $coord    = '_coordY';
                $operator = '+';
                break;
            case 'R':
                $coord    = '_coordX';
                $operator = '+';
                break;
            case 'D':
                $coord    = '_coordY';
                $operator = '-';
                break;
            case 'L':
                $coord    = '_coordX';
                $operator = '-';
                break;
            default:
                return;
        }

        if ($operator == '+') {
            $this->$coord += 1;
        } else {
            $this->$coord -= 1;
        }

        // Revert if last step lead outside the matrix
        if (!isset($this->_matrix[$this->_coordX . '|' . $this->_coordY])) {
            if ($operator == '+') {
                $this->$coord -= 1;
            } else {
                $this->$coord += 1;
            }
        }
    }
}
