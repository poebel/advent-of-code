<?php

class Solver_2016_03
{
    public function solve1($input)
    {
        $input = explode("\n", $input);

        $result = 0;

        foreach ($input as $row) {
            $row = trim($row);
            $row = preg_replace('/\s+/', ' ', $row);

            if ($this->_isValidTriangle($row)) {
                $result += 1;
            }
        }

        return $result;
    }

    public function solve2($input)
    {
        $input = explode("\n", $input);

        $result = 0;

        $triangles = array(
            array(),
            array(),
            array(),
        );

        foreach ($input as $i => $row) {
            if (!($i % 3)) {
                foreach ($triangles as $triangle) {
                    if ($this->_isValidTriangle($triangle)) {
                        $result += 1;
                    }
                }

                $triangles = array(
                    array(),
                    array(),
                    array(),
                );
            }

            $row = trim($row);
            $row = preg_replace('/\s+/', ' ', $row);
            $row = explode(' ', $row);

            $triangles[0][] = array_shift($row);
            $triangles[1][] = array_shift($row);
            $triangles[2][] = array_shift($row);
        }

        foreach ($triangles as $triangle) {
            if ($this->_isValidTriangle($triangle)) {
                $result += 1;
            }
        }

        return $result;
    }

    protected function _isValidTriangle($input)
    {
        if (!is_array($input)) {
            $input = explode(' ', $input);
        }

        sort($input);
        $min = array_shift($input);
        $mid = array_shift($input);
        $max = array_shift($input);

        return ($min + $mid > $max);
    }
}
