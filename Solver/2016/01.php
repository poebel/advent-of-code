<?php

class Solver_2016_01
{
    protected $_coordX         = 0;
    protected $_coordY         = 0;
    protected $_direction      = 'N';
    protected $_directionOrder = array('N', 'E', 'S', 'W');
    protected $_directionCalc
                               = array(
            'N' => array('coord' => 'Y', 'operator' => '+'),
            'E' => array('coord' => 'X', 'operator' => '+'),
            'S' => array('coord' => 'Y', 'operator' => '-'),
            'W' => array('coord' => 'X', 'operator' => '-'),
        );

    protected $_visitedLocations = array('0|0' => true);

    public function solve1($input)
    {
        return $this->calc($input);
    }

    public function solve2($input)
    {
        return $this->calc($input, true);
    }

    public function calc($input, $returnOnDuplicate = false)
    {
        $this->_reset();

        $input = explode(',', $input);

        foreach ($input as $step) {
            preg_match('/\w/', $step, $turn);
            preg_match('/\d+/', $step, $steps);
            $turn  = array_shift($turn);
            $steps = array_shift($steps);

            $this->_setNextDirection($turn);
            if ($this->_move($steps, $returnOnDuplicate)) {
                return $this->_calcDistance();
            }
        }

        return $this->_calcDistance();
    }

    protected function _reset()
    {
        $this->_coordX           = 0;
        $this->_coordY           = 0;
        $this->_direction        = 'N';
        $this->_visitedLocations = array('0|0' => true);
    }

    protected function _calcDistance()
    {
        $distance1 = 0 - $this->_coordX;
        $distance2 = 0 - $this->_coordY;

        if ($distance1 < 0) {
            $distance1 *= -1;
        }

        if ($distance2 < 0) {
            $distance2 *= -1;
        }

        return $distance1 + $distance2;
    }

    protected function _move($steps, $returnOnDuplicate = false)
    {
        $moveDefinition = $this->_directionCalc[$this->_direction];

        $coordName = '_coord' . $moveDefinition['coord'];

        for ($i = 1; $i <= $steps; $i++) {
            if ($moveDefinition['operator'] == '+') {
                $this->$coordName += 1;
            } else {
                $this->$coordName -= 1;
            }

            $locationKey = $this->_coordX . '|' . $this->_coordY;
            if (isset($this->_visitedLocations[$locationKey]) && $returnOnDuplicate) {
                return true;
            } else {
                $this->_visitedLocations[$locationKey] = true;
            }
        }

        return false;
    }

    protected function _setNextDirection($turn)
    {
        $directionKey = array_search($this->_direction, $this->_directionOrder);

        if ($turn == 'R') {
            $directionKey += 1;
        } else {
            $directionKey -= 1;
        }

        if ($directionKey >= count($this->_directionOrder)) {
            $directionKey = 0;
        }

        if ($directionKey < 0) {
            $directionKey = (count($this->_directionOrder) - 1);
        }

        $this->_direction = $this->_directionOrder[$directionKey];
    }
}
