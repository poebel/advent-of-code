<?php

class Solver_2016_06
{
    public function solve1($input)
    {
        return $this->_solve($input);
    }

    public function solve2($input)
    {
        return $this->_solve($input, false);
    }

    protected function _solve($input, $reverse = true)
    {
        $result = [];

        $input = explode("\n", $input);

        foreach ($input as $row) {
            $row = str_split(trim($row));

            foreach ($row as $pos => $letter) {
                if (!isset($result[$pos])) {
                    $result[$pos] = [];
                }
                if (!isset($result[$pos][$letter])) {
                    $result[$pos][$letter] = 0;
                }
                $result[$pos][$letter] += 1;
            }
        }

        $resultText = [];

        foreach ($result as $letters) {
            if ($reverse) {
                arsort($letters);
            } else {
                asort($letters);
            }
            $resultText[] = key($letters);
        }

        return implode('', $resultText);
    }
}
