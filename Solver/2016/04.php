<?php

class Solver_2016_04
{
    public function solve1($input)
    {
        $input = explode("\n", $input);

        $result = 0;

        foreach ($input as $row) {
            $room = $this->_rowToRoom($row);

            if ($this->_isValidRoom($room['name'], $room['checksum'])) {
                $result += $room['sector_id'];
            }
        }

        return $result;
    }

    public function solve2($input)
    {
        $input = explode("\n", $input);

        foreach ($input as $row) {
            $room = $this->_rowToRoom($row, true);

            if ($this->_isValidRoom($room['name'], $room['checksum']) && $room['name_decrypted'] == 'northpole object storage') {
                return $room['sector_id'];
            }
        }

        return 'ERROR';
    }

    protected function _rowToRoom($row, $decryptName = false)
    {
        $row      = strtolower(trim($row));
        $name     = substr($row, 0, strlen($row) - 7);
        $checksum = substr($row, strpos($row, '[') + 1, 5);
        $name     = explode('-', $name);
        $sectorId = array_pop($name);
        $name     = implode('-', $name);

        return [
            'name'           => $name,
            'name_decrypted' => ($decryptName ? $this->_decryptName($name, $sectorId) : null),
            'checksum'       => $checksum,
            'sector_id'      => $sectorId,
        ];
    }

    protected function _decryptName($name, $sectorId)
    {
        return str_replace('-', ' ', $this->_strRot($name, $sectorId));
    }

    protected function _isValidRoom($name, $checksum)
    {
        $name = str_replace('-', '', $name);

        $letterCount = [];
        $name        = str_split($name);
        foreach ($name as $letter) {
            if (!isset($letterCount[$letter])) {
                $letterCount[$letter] = 0;
            }
            $letterCount[$letter] += 1;
        }

        arsort($letterCount);

        $lettersByCount = [];
        foreach ($letterCount as $letter => $count) {
            if (!isset($lettersByCount[$count])) {
                $lettersByCount[$count] = [];
            }
            $lettersByCount[$count][] = $letter;
        }

        $resultChecksum = [];
        foreach ($lettersByCount as $letters) {
            sort($letters);
            while (!empty($letters) && count($resultChecksum) < 5) {
                $resultChecksum[] = array_shift($letters);
            }

            if (count($resultChecksum) >= 5) {
                break;
            }
        }
        $resultChecksum = implode('', $resultChecksum);

        return ($checksum == $resultChecksum);
    }

    protected function _strRot($s, $n)
    {
        static $letters = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz';

        $n = (int)$n % 26;
        if (!$n) {
            return $s;
        }

        if ($n < 0) {
            $n += 26;
        }

        if ($n == 13) {
            return str_rot13($s);
        }

        $rep = substr($letters, $n * 2) . substr($letters, 0, $n * 2);

        return strtr($s, $letters, $rep);
    }
}
