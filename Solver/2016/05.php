<?php

class Solver_2016_05
{
    public function solve1($input)
    {
        $input = trim($input);

        $result = [];

        $i = 1;
        while (count($result) < 8) {
            $hash = md5($input . $i);
            if (strpos($hash, '00000') === 0) {
                $result[] = substr($hash, 5, 1);
            }
            $i++;
        }

        return implode('', $result);
    }

    public function solve2($input)
    {
        $input = trim($input);

        $result = [];

        $i = 1;
        while (count($result) < 8) {
            $hash = md5($input . $i);
            if (strpos($hash, '00000') === 0) {
                $pos    = substr($hash, 5, 1);
                $intPos = (int)$pos;
                $char   = substr($hash, 6, 1);

                if ($pos === '0' || ($intPos >= 1 && $intPos <= 7)) {
                    if (!isset($result[$intPos])) {
                        $result[$intPos] = $char;
                    }
                }
            }
            $i++;
        }

        ksort($result);

        return implode('', $result);
    }
}
