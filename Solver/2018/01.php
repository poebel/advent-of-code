<?php

class Solver_2018_01
{
    public function solve1($input)
    {
        $input = trim($input);
        $input = explode("\n", $input);
        $result = 0;

        foreach ($input as $row) {
            $subtract = false;
            if (strpos($row, '-') !== false) {
                $subtract = true;
            }

            $row = (int)str_replace(['-', '+'], ['', ''], $row);

            if ($subtract) {
                $result -= $row;
            } else {
                $result += $row;
            }
        }

        return $result;
    }

    public function solve2($input)
    {
        $input = trim($input);
        $input = explode("\n", $input);
        $result = 0;
        $foundResults = [];

        $i = 0;
        while (true) {
            if ($i >= count($input)) {
                $i = 0;
            }

            $row = $input[$i++];

            $subtract = false;
            if (strpos($row, '-') !== false) {
                $subtract = true;
            }

            $row = (int)str_replace(['-', '+'], ['', ''], $row);

            if ($subtract) {
                $result -= $row;
            } else {
                $result += $row;
            }

            if (in_array($result, $foundResults)) {
                return $result;
            }

            $foundResults[] = $result;
        }
    }
}
