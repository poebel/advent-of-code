<?php
define('DS', DIRECTORY_SEPARATOR);

spl_autoload_register(
    function ($className) {
        require_once str_replace('_', DS, $className) . '.php';
    }
);

$solverYears = glob('Solver' . DS . '*', GLOB_ONLYDIR);
foreach ($solverYears as $solverYear) {
    $year = basename($solverYear);
    foreach (glob($solverYear . DS . '*.php') as $solverDay) {
        $day           = basename($solverDay, '.php');
        $date          = $year . '_' . $day;
        $solver        = false;
        $input         = false;
        $requireSolver = true;
        $result1       = 'ERROR';
        $resultFile1   = 'Results' . DS . $year . DS . $day . '.1.txt';
        $result2       = 'ERROR';
        $resultFile2   = 'Results' . DS . $year . DS . $day . '.2.txt';
        $resultDir     = dirname($resultFile1);

        if (!is_dir($resultDir)) {
            mkdir($resultDir, 0755, true);
        }

        if (file_exists($resultFile1) && file_exists($resultFile2)) {
            $requireSolver = false;
        }

        if ($requireSolver) {
            $inputFile = 'Inputs' . DS . $year . DS . $day . '.txt';
            if (!file_exists($inputFile)) {
                echo 'ERROR :: Missing input for "' . $year . '/' . $day . '"' . PHP_EOL;
                continue;
            }

            $input      = trim(file_get_contents($inputFile));
            $solverName = 'Solver_' . $date;
            $solver     = new $solverName();
        }

        $start = microtime(true);

        if (file_exists($resultFile1)) {
            $result1 = file_get_contents($resultFile1);
        } elseif ($solver) {
            $result1 = $solver->solve1($input);
            if ($result1 && in_array('save', $argv)) {
                file_put_contents($resultFile1, $result1);
            }
        }

        if (file_exists($resultFile2)) {
            $result2 = file_get_contents($resultFile2);
        } elseif ($solver) {
            $result2 = $solver->solve2($input);
            if ($result2 && in_array('save', $argv)) {
                file_put_contents($resultFile2, $result2);
            }
        }

        $duration = microtime(true) - $start;

        printf('%s :: %05.2f :: %10d :: %10d' . PHP_EOL, str_replace('_', '-', $date), $duration, $result1, $result2);

        unset($input);
        unset($solver);
    }
    echo PHP_EOL;
}
