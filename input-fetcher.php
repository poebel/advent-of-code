<?php

$baseUrl = 'https://adventofcode.com/%d/day/%d/input';
$invalidInputCheck = 'endpoint before it unlocks!';

foreach (glob('Inputs/*', GLOB_ONLYDIR) as $yearDirectory) {
    $year = (int)basename($yearDirectory);
    for ($day = 1; $day <= 25; $day++) {
        if($year > date('Y') || ($year == date('Y') && $day > date('d'))) {
            continue;
        }

        printf('Fetching %02d / %04d ...', $day, $year);

        $inputFile = sprintf('%s/%02d.txt', $yearDirectory, $day);
        if (file_exists($inputFile)) {
            $input = file_get_contents($inputFile);
            if (stripos($input, $invalidInputCheck) == false) {
                echo ' already exists, skipping.' . PHP_EOL;
                continue;
            }
        }

        $url = sprintf($baseUrl, $year, $day);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIE, $argv[1]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        file_put_contents($inputFile, rtrim(curl_exec($ch), "\n\r"));
        echo ' done.' . PHP_EOL;
    }
}